import java.util.Scanner;


public class Main {
    /**
 * Класс по реалізації чисел фібоначі
 */

    public static int fmax(int[] array) {
        /**
     <u>   знаходження максимального числа фібоначі з парних значень </u>
     */

        int fibonachi2 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((fibonachi2 < array[i]) && array[i] % 2 == 0) {
                fibonachi2 = array[i];

            }
        }
        return fibonachi2;
    }

    public static int fmaxn(int[] array)  {
        /**
     <u>   знаходження максимального числа фібоначі з непарних значень </u>
     */
        int fibonachi1 = 0;
        for (int i = 0; i < array.length; i++) {
            if ((fibonachi1 < array[i]) && array[i] % 2 == 1) {
                fibonachi1 = array[i];

            }
        }
        return fibonachi1;
    }

    public static void napovnennya(int[] array) {/**
     <u>  банальне наповненння числами фібоні масиву  </u>
     */
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < array.length; i++) {
            array[i] = array[i - 2] + array[i - 1];
        }


    }

    public static void main(String[] args) {
        System.out.println(" Введіть кількість ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        final int[] array = new int[size];
        napovnennya(array);
        int fibonachi1 = fmax(array);
        int fibonachi2 = fmaxn(array);
        System.out.println(fibonachi2 + " максимальне непарне число ");
        System.out.println(fibonachi1 + " максимальне парне число ");

    }
}
